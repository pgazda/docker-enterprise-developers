### Install Docker EE

From [https://docs.docker.com/install/linux/docker-ee/centos/](https://docs.docker.com/install/linux/docker-ee/centos/)

##### Uninstall Docker CE

* Remove packages and old repositories
```
sudo yum remove docker-ce docker-ce-cli
sudo rm /etc/yum.repos.d/docker*.repo

```
##### Start Docker EE trial

* Start your Docker EE trial here:
[https://hub.docker.com/editions/enterprise/docker-ee-trial](https://hub.docker.com/editions/enterprise/docker-ee-trial)

* Get your unique URL from hub (change user for your username):
[https://hub.docker.com/u/user/content](https://hub.docker.com/u/user/content)

* Create DOCKERURL variable with your unique URL and export it to yum variables
```
export DOCKERURL="<DOCKER-EE-URL>"
sudo -E sh -c 'echo "$DOCKERURL/centos" > /etc/yum/vars/dockerurl'
sudo -E yum-config-manager \
    --add-repo \
    "$DOCKERURL/centos/docker-ee.repo"
```
##### Install Docker EE

* Install packages
```
sudo yum -y install docker-ee docker-ee-cli containerd.io

```
### Install UCP

##### Prepare prerequisites

* Open required ports on all nodes
```
sudo firewall-cmd --add-port=179/tcp \
                                  --add-port=443/tcp \
                                  --add-port=6443/tcp \
                                  --add-port=6444/tcp \
                                  --add-port=10250/tcp \
                                  --add-port=12376/tcp \
                                  --add-port=12378-12388/tcp \
                                  --permanent
```
* Restart firewall and Docker
```
sudo firewall-cmd --reload
sudo systemctl restart docker

```

##### Install UCP

* Download required image
```
docker image pull docker/ucp:3.1.3

```
* Finally install UCP
```
docker container run --rm -it --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:3.1.3 install \
  --host-address <node-ip-address> \
  --interactive

```
* After the installation is finished, download your license key from "My Content" on [hub.docker.com](hub.docker.com) and upload it to UCP.

### Install Docker Trusted Registry

* Once UCP is installed, navigate to the UCP web UI. In the Admin Settings, choose Docker Trusted Registry.
* Fill out DTR external URL (https://your.ip) and other options and you'll get a snippet you can use
* Pull the image
```
docker pull docker/dtr:2.6.2

```

* Install DTR (use the snippet generated in previous step)
```
docker run -it --rm \
  docker/dtr:2.6.2 install \
  --ucp-node osboxes \
  --ucp-username user \
  --ucp-url https://ip.address \
  --ucp-insecure-tls \
  --replica-http-port 81 --replica-https-port 4443 \
  --dtr-external-url https://ip.address:4443

```
##### Login to DTR

* Before you login, you need to import DTR certificate
```
sudo curl -k https://ip.address:4443/ca -o /etc/pki/ca-trust/source/anchors/ip.address:4443.crt
sudo update-ca-trust
sudo systemctl restart docker

```
* Login to DTR
```
docker login ip.address:4443

```
* Repeat for all nodes in swarm

##### Test upload of image

* Create repository in DTR (https://ip.address:4443)
* Retag image
```
docker image pull alpine:latest
docker image tag alpine:latest ip.address:4443/user/alpine:latest

```
* Push it
```
docker image push ip.address:4443/user/alpine:latest

```

### For troubleshooting

##### Leave existing swarm
```
docker swarm leave
```

or if it's not enough
```
docker swarm leave --force

```

##### Starting fresh

* List of ports needed for swarm
```
sudo firewall-cmd --add-port=2376/tcp \
			 --add-port=2377/tcp \
             --add-port=7946/tcp \
             --add-port=7946/udp \
			 --add-port=4789/udp \
			 --permanent
```
* Restart firewall and docker
```
sudo firewall-cmd --reload
sudo systemctl restart docker

```

-
